const prioritySelect = document.getElementById("select-line");
const priorityShow = document.getElementById("select-view");
const addTaskButton = document.getElementById("add-task-div");
const applyButton = document.getElementById("down-buttons-apply-button");
const applyButtonText = document.getElementById("down-buttons-apply-button-text");
const deleteButton = document.getElementById("down-buttons-delete-button");
const mainSection = document.getElementById("main-section");
const sideSection = document.getElementById("side-section");
const closeSideSectionButton = document.getElementById("side-section-close-button");
const taskName = document.getElementById("task-name");
const taskDescription = document.getElementById("task-description");
const dueDate = document.getElementById("date-input");
let taskCollection = document.querySelectorAll(".task");
let taskDescriptionCollection = [""];
let lastClickedTask;
let lastClickedTaskIndex;
repeat();

function hideSideSection()
{
    mainSection.style.transform = "translateX(0px)";
    sideSection.style.transform = "translateY(-112%)";
    setTimeout(() => {sideSection.style.display = "none";}, 800); 
}

function showSideSection()
{
    mainSection.style.transform = "translateX(-250px)";
    sideSection.style.display = "block";
    setTimeout(() => {sideSection.style.transform = "translateY(112%)";}, 0);
}

function clearFields()
{
    let arrowDown = document.createElement("i");
    deleteButton.style.display = "none";
    applyButtonText.innerText = "Add task";
    taskName.value = "";
    taskDescription.value = "";
    prioritySelect.innerText = "Priority 1";
    arrowDown.classList.add("fa-solid");
    arrowDown.classList.add("fa-angle-down");
    arrowDown.style.backgroundColor = "red";
    prioritySelect.style.backgroundColor = "red";
    prioritySelect.appendChild(arrowDown);
    dueDate.value = null;
}

function repeat(element, index)
{
    taskCollection.forEach((element, index, array)=>{
        element.addEventListener("mousedown", (event) => {
            console.log(taskDescriptionCollection);
            taskName.classList.remove("highlighted");
            if (lastClickedTaskIndex != null) 
            {
                lastClickedTask.classList.remove("selected");
                lastClickedTask.querySelector(".arrow-right-button").classList.remove("selected");
            }
            lastClickedTask = element;
            lastClickedTaskIndex = index;
            if (event.target.classList.contains("check-box")) 
            {   
                lastClickedTask.classList.add("disappearing");
                setTimeout(() => {
                    mainSection.removeChild(lastClickedTask);
                    clearFields();
                }, 1050);
                return;
            }
            lastClickedTask.classList.add("selected");
            lastClickedTask.querySelector(".arrow-right-button").classList.add("selected")
            let flagColor = window.getComputedStyle(element.querySelector(".fa-flag")).color;
            let arrowDown = document.createElement("i");
            let date = "";
            if (element.querySelector(".due-date") != null)  
            date = element.querySelector(".due-date").innerText;

            showSideSection();
            deleteButton.style.display = "flex";
            applyButtonText.innerText = "Apply changes";
            taskName.value = element.querySelector(".task-name").innerText;
            console.log(taskDescriptionCollection[index]);
            taskDescription.value = taskDescriptionCollection[index];
            prioritySelect.innerText = flagColor == "rgb(255, 0, 0)" ? "Priority 1" : flagColor == "rgb(255, 255, 0)" ?
                                                                        "Priority 2" : "Priority 3";
            arrowDown.classList.add("fa-solid");
            arrowDown.classList.add("fa-angle-down");
            arrowDown.style.backgroundColor = flagColor;
            prioritySelect.style.backgroundColor = flagColor;
            prioritySelect.appendChild(arrowDown);
            dueDate.value = date;
        });
    })
}

taskName.onclick = () => {
    taskName.classList.remove("highlighted");
}

applyButton.onclick = () => {

    if (taskName.value == "")
    {
        taskName.classList.remove("highlighted");
        setTimeout(() => {taskName.classList.add("highlighted");}, 0);
        return;
    }
    if (applyButtonText.innerText == "Add task")
    {
        let hr = document.createElement("div");
        let newTask = document.createElement("div");
        let firstLine = document.createElement("div");
        let secondLine = document.createElement("div");
        let checkBox = document.createElement("input");
        let name = document.createElement("p");
        let buttonArrowRight = document.createElement("button");
        let arrowIcon = document.createElement("i");
        let calendarIcon = document.createElement("i");
        let date = document.createElement("p");
        let priorityFlag = document.createElement("i");

        hr.classList.add("hr");
        newTask.classList.add("task");
        firstLine.classList.add("task-first-line");
        secondLine.classList.add("task-second-line");
        checkBox.classList.add("check-box");
        name.classList.add("task-name");
        buttonArrowRight.classList.add("arrow-right-button");
        arrowIcon.classList.add("fa-solid", "fa-angle-right");
        calendarIcon.classList.add("fa-solid", "fa-calendar-days");
        date.classList.add("due-date");
        priorityFlag.classList.add("fa-solid", "fa-flag");
        checkBox.type = "checkbox";

        name.innerText = " ";
        name.innerText = taskName.value;
        date.innerText = dueDate.value;
        priorityFlag.style.color = prioritySelect.innerText == "Priority 1" ? "red" : 
                                prioritySelect.innerText == "Priority 2" ? "yellow" 
                                : "green";
        taskDescriptionCollection.push(taskDescription.value);

        buttonArrowRight.appendChild(arrowIcon);
        firstLine.appendChild(checkBox);
        firstLine.appendChild(name);
        firstLine.appendChild(buttonArrowRight);
        if (date.innerText != "")
        {
            secondLine.appendChild(calendarIcon);
            secondLine.appendChild(date);
        }
        secondLine.appendChild(priorityFlag);
        newTask.appendChild(hr);
        newTask.appendChild(firstLine);
        newTask.appendChild(secondLine);
        mainSection.appendChild(newTask);
        taskCollection = document.querySelectorAll(".task");
        repeat();
        console.log(taskDescriptionCollection);
        clearFields();
    }
    else
    {   
        console.log(taskDescriptionCollection);
        lastClickedTask.querySelector(".task-name").innerText = taskName.value;
        taskDescriptionCollection[lastClickedTaskIndex] = taskDescription.value;
        if (lastClickedTask.querySelector(".due-date") == null && dueDate.value != null)
        {
                let date = document.createElement("p");
                let calendarIcon = document.createElement("i");
                calendarIcon.classList.add("fa-solid", "fa-calendar-days");
                date.classList.add("due-date");
                date.innerText = dueDate.value;
                lastClickedTask.querySelector(".task-second-line").insertBefore(date, lastClickedTask.querySelector(".task-second-line").querySelector(".fa-flag"));
                lastClickedTask.querySelector(".task-second-line").insertBefore(calendarIcon, lastClickedTask.querySelector(".task-second-line").querySelector(".due-date"));
        }
        lastClickedTask.querySelector(".fa-flag").style.color = prioritySelect.innerText == "Priority 1" ? "red" :
        prioritySelect.innerText == "Priority 2" ? "yellow" : "green";
    }
}

closeSideSectionButton.onclick = () => {
    if (lastClickedTaskIndex != null) 
    {
        lastClickedTask.classList.remove("selected");
        lastClickedTask.querySelector(".arrow-right-button").classList.remove("selected");
    }
    hideSideSection();
}

addTaskButton.onclick = () => {
    showSideSection();
    if (lastClickedTaskIndex != null) 
    {
        lastClickedTask.classList.remove("selected");
        lastClickedTask.querySelector(".arrow-right-button").classList.remove("selected");
    }
    clearFields();
}

priorityShow.addEventListener("mousedown", e => {
    prioritySelect.innerText = e.target.innerText;
    let arrowDown = document.createElement("i");
    arrowDown.classList.add("fa-solid");
    arrowDown.classList.add("fa-angle-down");
    arrowDown.style.backgroundColor = e.target.id == "first-li" ? "red" :  e.target.id == "second-li" ? "yellow" : "green";
    prioritySelect.appendChild(arrowDown);
    prioritySelect.style.backgroundColor = arrowDown.style.backgroundColor;
})

document.addEventListener("mousedown", event => {
    if (!prioritySelect.contains(event.target)) 
    {
        priorityShow.style.display = "none";
    }
    else
    {
        priorityShow.style.display = "block";
    }
})

deleteButton.onclick = () => {
    mainSection.removeChild(lastClickedTask);
    clearFields();
}