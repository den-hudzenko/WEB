 <?php
    include "config.php";
    header("Content-Type: application/json");
    header("Access-Control-Allow-Origin: *");
    header("Cache-Control: no-cache");

    function handle_error($code, $message)
    {
        $response["status"] = false;
        $response["error"]["code"] = $code;
        $response["error"]["message"] = $message;
        echo json_encode($response);
        exit;
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST['group'])) {
            handle_error(101, "Choose the group from the dropdown list");
        } else if (!array_key_exists($_POST['group'], $groupArr)) {
            handle_error(102, "Invalid group");
        }
        if (empty($_POST['firstName'])) {
            handle_error(103, "First name field can not be empty");
        }
        if (empty($_POST['lastName'])) {
            handle_error(104, "Last name field can not be empty");
        }
        if (empty($_POST['gender'])) {
            handle_error(105, "Choose the gender from the dropdown list");
        } else if (!array_key_exists($_POST['gender'], $genderArr)) {
            handle_error(106, "Invalid gender");
        }
        if (empty($_POST['birthday']) || strtotime($_POST['birthday']) === false) {
            handle_error(107, "Choose the date of birth from the dropdown calendar");
        }

        $id = $_POST['id'];
        $group = $_POST['group'];
        $first_name = $_POST['firstName'];
        $last_name = $_POST['lastName'];
        $gender = $_POST['gender'];
        $birthday = date('Y-m-d', strtotime($_POST['birthday']));
        $status = $_POST['status'] === 'true';

        if ($id) {
            $stmt = $conn->prepare("UPDATE students SET `group_id` = :group, `first_name` = :first_name, `last_name`= :last_name,
                                        `gender_id` = :gender, `birthday` = :birthday, `status` = :status WHERE `id` = :id");
        } else {
            $stmt = $conn->prepare("INSERT INTO students (`group_id`, `first_name`, `last_name`, `gender_id`, `birthday`, `status`) 
                                        VALUES (:group, :first_name, :last_name, :gender, :birthday, :status)");
        }

        $stmt->bindParam(':group', $group);
        $stmt->bindParam(':first_name', $first_name);
        $stmt->bindParam(':last_name', $last_name);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':birthday', $birthday);
        $stmt->bindParam(':status', $status, PDO::PARAM_BOOL);

        try {
            if ($id === "") {
                $stmt->execute();
                $response["id"] = $conn->lastInsertId();
            } else {
                $stmt->bindParam(':id', $id);
                $stmt->execute();
                $response["id"] = $id;
            }
        } catch (PDOException $e) {
            handle_error($e->getCode(), $e->getMessage());
        }

        $response["status"] = true;
        echo json_encode($response);
        exit;
    }

    http_response_code(403);
    echo "Requested resource is forbidden";
