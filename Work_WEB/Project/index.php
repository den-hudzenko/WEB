 <!DOCTYPE html>
 <html lang="uk">

 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- <link rel="manifest" href="/manifest.json"> -->
     <title>Students</title>
     <link rel="stylesheet" href="style.css">
 </head>

 <body>
     <?php include("config.php"); ?>

     <header>
         <div id="cms">CMS</div>

         <div class="dropdown-container">

             <div class="notify-dropdown">

                 <a href="messages.php" id="open-messages">
                     <button id="notificationBtn">
                         <i class="fa-regular fa-bell fa-xl" id="bellIcon"></i>
                         <span class="icon-button-badge"></span>
                     </button>
                 </a>

                 <div class="notify-content">
                     <a href="mailto: johnsmith@gmail.com">
                         <i class="fa-regular fa-user"></i>
                         John: What`s up?
                     </a>
                     <a href="mailto:annbond@gmail.com">
                         <i class="fa-regular fa-user"></i>
                         Ann: Hello!
                     </a>
                 </div>
             </div>

             <div class="user-dropdown">
                 <button id="userBtn">
                     <img id="profilePicture" src="images/avatar.png" alt="Profile picture">
                     <h4 id="username">Caped Crusaider</h4>
                 </button>
                 <div class="user-content">
                     <a href="#">Profile</a>
                     <a href="#">Log out</a>
                 </div>
             </div>

         </div>

     </header>

     <main>

         <div class="navigation-container">
             <input type="checkbox" class="toggle" id="toggle-checkbox">
             <label for="toggle-checkbox" class="toggle-label"><i class="fa-solid fa-bars"></i></label>

             <nav class="navbar">
                 <ul>
                     <li><a href="#">Profile</a></li>
                     <li><a href="dashboard.php">Dashboard</a></li>
                     <li><a href="index.php" class="active">Students</a></li>
                     <li><a href="tasks.php">Tasks</a></li>
                     <li><a href="#">Log out</a></li>
                 </ul>
             </nav>
         </div>

         <?php if (isset($connectionError)) : ?>
             <div class="server-message"><?php echo $connectionError; ?></div>
         <?php endif; ?>

         <h1>Students</h1>

         <button class="button open-button row-action" data-row-action-id="" data-bs-toggle="modal" data-bs-target="#exampleModal">
             <i class="fa-solid fa-plus"></i>
         </button>

         <div class="table-container">
             <table id="studentsTable">
                 <thead id="tableHead">
                     <tr>
                         <th><input type="checkbox" id="selectAll"></th>
                         <th class="group">Group</th>
                         <th class="name">Name</th>
                         <th class="gender">Gender</th>
                         <th class="birthday">Birthday</th>
                         <th class="status">Status</th>
                         <th>Options</th>
                     </tr>
                 </thead>

                 <tbody id="tableBody">

                     <?php $result = getStudents(); ?>

                     <?php if (isset($fetchError)) { ?>
                         <div class="server-message">
                             <p><?php echo $fetchError; ?></p>
                         </div>
                     <?php } ?>

                     <?php foreach ($result as $row) {
                            $row['birthday'] = date('d.m.Y', strtotime($row['birthday'])) ?>

                         <tr id="<?= $row["id"]; ?>" data-id="<?= $row["id"]; ?>" data-group="<?= $row["group_id"]; ?>" data-first-name="<?= $row["first_name"]; ?>" data-last-name="<?= $row["last_name"]; ?>" data-gender="<?= $row["gender_id"]; ?>" data-birthday="<?= $row["birthday"]; ?>" data-status="<?= $row["status"] ? 'true' : 'false'; ?>">

                             <td><input type='checkbox'></td>
                             <td class='group'><?= $groupArr[$row["group_id"]]; ?></td>
                             <td class='name'><?= $row["first_name"] . " " . $row["last_name"]; ?></td>
                             <td class='gender'><?= $genderArr[$row["gender_id"]]; ?></td>
                             <td class='birthday'><?= $row["birthday"]; ?></td>
                             <td><span class='status-dot <?= $row["status"] = $row["status"] ? "active-dot" : ""; ?>'></span></td>
                             <td>
                                 <button class='row-action' data-row-action-id="<?= $row["id"]; ?>">
                                     <i class='fa-solid fa-pencil'></i>
                                 </button>
                                 <button class='delete-row' data-row-id="<?= $row['id']; ?>">
                                     <i class='fa-solid fa-xmark fa-lg'></i>
                                 </button>
                             </td>
                         </tr>
                     <?php } ?>
                 </tbody>

             </table>
         </div>

         <div class="pagination">
             <a href="#">&langle;</a>
             <a href="#" class="active">1</a>
             <a href="#">&rangle;</a>
         </div>

     </main>
 </body>

 <dialog class="modal" id="modal">
     <div class="dialog-header">
         <h3>Add student</h3>
         <button class="close-modal">
             <i class="fa-solid fa-xmark fa-lg"></i>
         </button>
     </div>

     <hr>

     <form class="form" method="post" action="submithandler.php">
         <input type="hidden" id="id">

         <div class="submit-message"></div>

         <div>
             <label for="group">Group</label>
             <select id="group" title="Select the group from the drop-down list">
                 <option value="" data-group-id="0">--Select Group--</option>
                 <?php foreach ($groupArr as $key => $group) { ?>
                     <option value="<?= $key ?>" data-group-id="<?= $key ?>"><?= $group ?></option>
                 <?php } ?>
             </select>
         </div>

         <div>
             <label for="first-name">First name</label>
             <input type="text" id="first-name" title="Only letters and '-'', ''' symbols are allowed" maxlength="25">
         </div>

         <div>
             <label for="last-name">Last name</label>
             <input type="text" id="last-name" title="Only letters and '-'', ''' symbols are allowed" maxlength="25">
         </div>

         <div>
             <label for="gender">Gender</label>
             <select id="gender" title="Select the gender from the drop-down list">
                 <option value="" data-gender-id="0">--Select Gender--</option>
                 <?php foreach ($genderArr as $key => $gender) { ?>
                     <option value="<?= $key ?>" data-gender-id="<?= $key ?>"><?= $gender ?></option>
                 <?php } ?>
             </select>
         </div>

         <div>
             <label for="birthday-date">Birthday</label>
             <input type="date" id="birthday-date" min="1930-01-01" max="2008-12-31" title="Select the birthday date from the drop-down calendar">
         </div>

         <div>
             <label for="status">Status</label>
             <label class="switch" title="Check to change the status">
                 <input type="checkbox" id="status">
                 <span class="slider round"></span>
             </label>
             <span class="status-text">Inactive</span>
         </div>

         <hr>

         <div class="dialog-submit-section">
             <button id="submit-data" type="submit">Add</button>
             <button class="close-modal">Cancel</button>
         </div>
     </form>
 </dialog>

 <dialog class="modal" id="delete-confirm-modal">
     <div class="dialog-header">
         <h3>Warning</h3>
         <button class="close-modal">
             <i class="fa-solid fa-xmark fa-lg"></i>
         </button>
     </div>

     <hr>

     <div class="delete-message"></div>
     <h4 id="confirm-delete-question"></h4>

     <hr>

     <div class="dialog-footer">
         <button class="cofirm-deleting-button">OK</button>
         <button class="close-modal">Close</button>
     </div>
 </dialog>

 </html>

 <script src="https://kit.fontawesome.com/d9209b8d96.js" crossorigin="anonymous"></script>
 <script type="module" src="script.js"></script>
 <!-- <script>
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function() {
        navigator.serviceWorker.register('/service-worker.js', { scope: '/' }).then(function(registration) {
          console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
          console.log('ServiceWorker registration failed: ', err);
        });
      });
    }
</script> -->