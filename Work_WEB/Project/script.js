// HTML ELEMENTS AND VARIABLES
const modal = document.querySelector("#modal");
const form = modal.querySelector(".form");
const confirmDelModal = document.querySelector("#delete-confirm-modal");
const studentsTable = document.querySelector("#studentsTable");
const tableBody = document.getElementById("tableBody");
const submitDataBtn = document.getElementById("submit-data");
const closeModalBtns = document.querySelectorAll(".close-modal");
const rowActionBtns = document.querySelectorAll(".row-action");
const deleteRowBtns = document.querySelectorAll(".delete-row");
const closeConfirmModalBtns = document.querySelectorAll(".close-confirm-modal");
const confirmDelBtn = confirmDelModal.querySelector(".cofirm-deleting-button");
const groupSelect = document.getElementById("group");
const genderSelect = document.getElementById("gender");
const submitMessageDisplay = document.querySelector(".submit-message");
const deleteMessageDisplay = document.querySelector(".delete-message");

// LISTENERS

// modal-opening listeners
rowActionBtns.forEach((button) => rowActionListener(button));

function rowActionListener(button) {
  button.addEventListener("click", function (event) {
    let dataObj = getEmptyObject();
    let rowId = this.getAttribute("data-row-action-id");

    if (rowId) {
      dataObj.id = rowId;
      const tableRow = document.getElementById(rowId);
      assignDataFromRow(dataObj, tableRow);
      changeModalTextForEditing();
    }

    fillModalFieldsWithData(dataObj);
    if (submitMessageDisplay.classList.contains("submit-invalid")) {
      submitMessageDisplay.classList.remove("submit-invalid");
    }
    modal.showModal();
  });
}

// submit request to php
function sendSubmitRequest(data) {
  const urlEncodedData = new URLSearchParams();
  for (const key in data) {
    urlEncodedData.append(key, data[key]);
  }
  return fetch("submit-handler.php", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: urlEncodedData,
  })
    .then((response) => response.json())
    .then((result) => {
      if (!result.status) {
        if (result.error && result.error.message) {
          submitMessageDisplay.classList.add("submit-invalid");
          submitMessageDisplay.textContent = result.error.message;
        }
      }
      return result;
    });
}

// submition listeners
if (submitDataBtn !== null) {
  submitDataBtn.addEventListener("click", async function (event) {
    event.preventDefault();
    const dataObj = getDataFromForm();

    sendSubmitRequest(dataObj).then(function (response) {
      if (response.status) {
        dataObj.id = response.id;
        addOrEditRow(dataObj);
        modal.close();
        resetModalTextForAdding();
      }
    });
  });
}

// delete listeners
deleteRowBtns.forEach((button) => deleteListener(button));

function deleteListener(button) {
  button.addEventListener("click", function () {
    const row = button.closest("tr");
    const fullName = row.querySelector(".name").textContent;
    const confirmMessage = `Are you sure you want to delete user ${fullName}?`;
    const confirmHeaderText = confirmDelModal.querySelector(
      "#confirm-delete-question"
    );
    confirmHeaderText.textContent = confirmMessage;
    confirmDelModal.showModal();
    confirmDelBtn.dataset.rowId = row.id;
  });
}

// confirm delete listener
if (confirmDelBtn !== null) {
  confirmDelBtn.addEventListener("click", function () {
    const rowId = this.dataset.rowId;
    const row = document.getElementById(rowId);
    if (row) {
      var dataObj = { id: rowId };
      sendDeleteRequest(dataObj).then(function (isValid) {
        if (isValid) {
          row.remove();
          confirmDelModal.close();
        }
      });
      if (deleteMessageDisplay.classList.contains("delete-invalid")) {
        deleteMessageDisplay.classList.remove("delete-invalid");
      }
    }
  });
}

// delete request to php
function sendDeleteRequest(data) {
  const urlEncodedData = new URLSearchParams();
  for (const key in data) {
    urlEncodedData.append(key, data[key]);
  }

  return fetch("delete-handler.php", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: urlEncodedData,
  })
    .then((response) => response.json())
    .then((result) => {
      if (!result.status) {
        if (result.error && result.error.message) {
          deleteMessageDisplay.classList.add("delete-invalid");
          deleteMessageDisplay.textContent = result.error.message;
        }
      }
      return result.status;
    });
}

// Additional functions
function assignDataFromRow(dataObj, row) {
  dataObj.group = row.getAttribute("data-group");
  dataObj.firstName = row.getAttribute("data-first-name");
  dataObj.lastName = row.getAttribute("data-last-name");
  dataObj.gender = row.getAttribute("data-gender");

  let birthday = row.getAttribute("data-birthday");
  let parts = birthday.split(".");
  dataObj.birthday = parts[2] + "-" + parts[1] + "-" + parts[0];
  dataObj.status = row.getAttribute("data-status") === "true";
}

function fillModalFieldsWithData(dataObj) {
  form.querySelector("#id").value = dataObj.id;
  form.querySelector("#group").value = dataObj.group;
  form.querySelector("#first-name").value = dataObj.firstName;
  form.querySelector("#last-name").value = dataObj.lastName;
  form.querySelector("#gender").value = dataObj.gender;
  form.querySelector("#birthday-date").value = dataObj.birthday;
  form.querySelector('input[type="checkbox"]').checked = dataObj.status;
}

function addOrEditRow(dataObj) {
  const newRow = getNewRowElementFromGivenData(dataObj);

  tableBody.appendChild(newRow);
  deleteListener(newRow.querySelector(".delete-row"));
  rowActionListener(newRow.querySelector(".row-action"));

  if (dataObj.id) {
    const row = document.getElementById(`${dataObj.id}`);
    if (row) {
      row.parentNode.replaceChild(newRow, row);
    }
  }

  if (Object.keys(dataObj).length !== 0) {
    const jsonString = JSON.stringify(dataObj, null, 2);
    console.log(jsonString);
  }
}

function getNewRowElementFromGivenData(dataObj) {
  const newRow = document.createElement("tr");

  newRow.id = dataObj.id;
  newRow.classList.add("tableRow");

  newRow.setAttribute("data-id", newRow.id);
  newRow.setAttribute("data-group", dataObj.group);
  newRow.setAttribute("data-first-name", dataObj.firstName);
  newRow.setAttribute("data-last-name", dataObj.lastName);
  newRow.setAttribute("data-gender", dataObj.gender);
  newRow.setAttribute("data-birthday", dataObj.birthday);
  newRow.setAttribute("data-status", dataObj.status);

  const status = dataObj.status ? "active-dot" : "";

  const selectedGroupOption = document.querySelector(
    `#group option[data-group-id="${dataObj.group}"]`
  );
  const groupText = selectedGroupOption ? selectedGroupOption.textContent : "";

  const selectedGenderOption = document.querySelector(
    `#gender option[data-gender-id="${dataObj.gender}"]`
  );
  const genderText = selectedGenderOption
    ? selectedGenderOption.textContent
    : "";

  newRow.innerHTML = `
      <td><input type="checkbox"></td>
      <td class="group">${groupText}</td>
      <td class="name">${dataObj.firstName} ${dataObj.lastName}</td>
      <td class="gender">${genderText}</td>
      <td class="birthday">${dataObj.birthday}</td>
      <td><span class="status-dot ${status}"></span></td>
      <td>
        <button class="row-action" data-row-action-id="${newRow.id}">
          <i class="fa-solid fa-pencil"></i>
        </button>
        <button class="delete-row">
          <i class="fa-solid fa-xmark fa-lg"></i>
        </button>
      </td>
  `;

  return newRow;
}

function getEmptyObject() {
  let emptyObject = {
    id: "",
    group: "",
    firstName: "",
    lastName: "",
    gender: "",
    birthday: "",
    status: false,
  };
  return emptyObject;
}

function getDataFromForm() {
  let dataObj = getEmptyObject();
  dataObj.id = form.querySelector("#id").value;
  dataObj.group = form.querySelector("#group").value;
  dataObj.firstName = form.querySelector("#first-name").value;
  dataObj.lastName = form.querySelector("#last-name").value;
  dataObj.gender = form.querySelector("#gender").value;
  let birthday = form.querySelector("#birthday-date").value;
  let parts = birthday.split("-");
  dataObj.birthday = parts[2] + "." + parts[1] + "." + parts[0];
  dataObj.status = form.querySelector('input[type="checkbox"]').checked
    ? true
    : false;
  return dataObj;
}

if (closeModalBtns !== null) {
  closeModalBtns.forEach((closeButton) => {
    closeButton.addEventListener("click", (event) => {
      event.preventDefault();
      const dialog = closeButton.closest("dialog");
      dialog.close();
    });
  });
}

// CHANGE TOGGLE TEXT
document.addEventListener("DOMContentLoaded", function () {
  const checkbox = document.querySelector('.switch input[type="checkbox"]');
  const statusText = document.querySelector(".status-text");

  checkbox.addEventListener("change", function () {
    statusText.textContent = this.checked ? "Active" : "Inactive";
  });
});

// CHANGE MODAL WINDOW TEXT
function changeModalTextForEditing() {
  const modalHeader = document.querySelector("#modal .dialog-header h3");
  const submitButton = document.querySelector("#modal #submit-data");

  modalHeader.textContent = "Edit student";
  submitButton.textContent = "Save";
}

function resetModalTextForAdding() {
  const modalHeader = document.querySelector("#modal .dialog-header h3");
  const submitButton = document.querySelector("#modal #submit-data");

  modalHeader.textContent = "Add student";
  submitButton.textContent = "Add";
}

console.log(22);
