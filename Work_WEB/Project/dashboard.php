 <!DOCTYPE html>
 <html lang="uk">

 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- <link rel="manifest" href="manifest.json"> -->
     <title>Students</title>
     <link rel="stylesheet" href="style.css">
 </head>

 <body>
     <header>
         <div id="cms">CMS</div>

         <div class="dropdown-container">

             <div class="notify-dropdown">
                 <a href="messages.php" id="open-messages">
                     <button id="notificationBtn">
                         <i class="fa-regular fa-bell fa-xl" id="bellIcon"></i>
                         <span class="icon-button-badge"></span>
                     </button>
                 </a>
                 <div class="notify-content">
                     <a href="mailto: johnsmith@gmail.com">
                         <i class="fa-regular fa-user"></i>
                         John: What`s up?
                     </a>
                     <a href="mailto:annbond@gmail.com">
                         <i class="fa-regular fa-user"></i>
                         Ann: Hello!
                     </a>
                 </div>
             </div>

             <div class="user-dropdown">
                 <button id="userBtn">
                     <img id="profilePicture" src="images/avatar.png" alt="Profile picture">
                     <h4 id="username">Caped Crusaider</h4>
                 </button>
                 <div class="user-content">
                     <a href="#">Profile</a>
                     <a href="#">Log out</a>
                 </div>
             </div>

         </div>

     </header>

     <main>

         <div class="navigation">
             <input type="checkbox" class="toggle" id="toggle-checkbox">
             <label for="toggle-checkbox" class="toggle-label"><i class="fa-solid fa-bars"></i></label>

             <nav class="navbar">
                 <ul>
                     <li><a href="#">Profile</a></li>
                     <li><a href="dashboard.php" class="active">Dashboard</a></li>
                     <li><a href="index.php">Students</a></li>
                     <li><a href="tasks.php">Tasks</a></li>
                     <li><a href="#">Log out</a></li>
                 </ul>
             </nav>
         </div>

         <h1>Dashboard</h1>

     </main>
 </body>

 </html>

 <script src="https://kit.fontawesome.com/d9209b8d96.js" crossorigin="anonymous"></script>