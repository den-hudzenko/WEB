const http = require("http");
const socketIo = require("socket.io");
const server = http.createServer();
const io = socketIo(server, { cors: { origin: "*" } });

const mongoose = require("mongoose");
const { User, Message, Relation } = require("./models");
const mongoDB = "mongodb://localhost:27017";
mongoose
  .connect(mongoDB)
  .then(() => {
    console.log("MongoDB connected successfully");
  })
  .catch((err) => console.log(err));

const PORT = 5000;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

const connections = [];
io.on("connection", (socket) => {
  connections.push(socket);

  socket.on("new-user", async (name) => {
    socket.username = name;
    try {
      await addUser(socket.id, name);
      const allUsers = await User.find().lean();
      if (!allUsers) {
        console.log("No users found");
      }
      const formattedUsers = {};
      allUsers.forEach((user) => {
        formattedUsers[user.socketId] = user.userName;
      });

      socket.broadcast.emit("new-chat", {
        id: socket.id,
        name: name,
        usersList: formattedUsers,
      });
      const relations = await Relation.find({ receiverId: null }).lean();
      const messageIds = relations.map((relation) => relation.messageId);
      const messages = await Message.find({ _id: { $in: messageIds } }).lean();

      socket.emit("user-connected", {
        id: socket.id,
        chatHistory: messages,
        usersList: formattedUsers,
      });
    } catch (error) {
      console.error("Error handling new user:", error);
    }
  });

  socket.on("send-chat-message", async (data) => {
    try {
      const message = new Message(data);
      await message.save();

      const senderUser = await User.findOne({ userName: data.senderName });
      let receiver = null;
      if (data.receiverName !== "0") {
        receiver = await User.findOne({ userName: data.receiverName });
      }

      const relation = new Relation({
        senderId: senderUser._id,
        receiverId: receiver ? receiver._id : null,
        messageId: message._id,
      });
      await relation.save();

      if (data.receiverName == "0") {
        socket.broadcast.emit("chat-message", data);
      } else {
        const targetSocket = connections.find(
          (conn) => conn.id === receiver.socketId
        );
        if (targetSocket) {
          socket.to(receiver.socketId).emit("chat-message", data);
        }
      }
    } catch (error) {
      console.error("Error handling chat message:", error);
    }
  });

  socket.on("get-chat-history", async (chatName, userName) => {
    try {
      const currentUser = await User.findOne({ userName: userName });
      if (!currentUser) {
        console.error("Current user not found");
        return;
      }
      let messages = [];

      if (chatName == "0") {
        const groupRelations = await Relation.find({ receiverId: null }).lean();
        const groupMessageIds = groupRelations.map(
          (relation) => relation.messageId
        );
        messages = await Message.find({ _id: { $in: groupMessageIds } }).lean();
      } else {
        const chatUser = await User.findOne({ userName: chatName });
        if (!chatUser) {
          console.error("Chat user not found");
          return;
        }
        const relations = await Relation.find({
          $or: [
            { senderId: currentUser._id, receiverId: chatUser._id },
            { senderId: chatUser._id, receiverId: currentUser._id },
          ],
        }).lean();

        const messageIds = relations.map((relation) => relation.messageId);
        messages = await Message.find({ _id: { $in: messageIds } }).lean();
      }

      socket.emit("chat-history", messages);
    } catch (error) {
      console.error("Error fetching chat history:", error);
    }
  });

  socket.on("disconnect", () => {
    const index = connections.indexOf(socket);
    if (index !== -1) {
      connections.splice(index, 1);
    }
  });
});

async function addUser(socketId, userName) {
  try {
    const existingUser = await User.findOne({ userName });
    if (!existingUser) {
      const user = new User({ socketId, userName });
      await user.save();
    } else {
      await User.updateOne({ userName }, { $set: { socketId } });
    }
  } catch (error) {
    console.error("Error adding user:", error);
  }
}
